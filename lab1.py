import urllib.request
import re

# 'http://www.csd.tsu.ru/'

answer = []
sites = ['http://www.csd.tsu.ru/']
dirty_breaker = 0


def parse_site(url):
    with urllib.request.urlopen(url) as response:
        html = response.read().decode("utf-8")
        mails = re.findall("[\w\.-]+@[\w-]+[\.-]+[\w]+", html)
        links = re.findall("<a[^>]+href=\"([^\#].*?)\"[^>]*>", html)
        global answer
        for x in mails:
            if x not in answer:
                answer.append(x)
        global sites
        sites += links
    return


while dirty_breaker < 5:
    parse_site(sites[dirty_breaker])
    dirty_breaker += 1

print(answer)
