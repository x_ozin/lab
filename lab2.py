import re

reg = "([1,2]?\d{1,2}\.[1,2]?\d{1,2}\.[1,2]?\d{1,2}\.[1,2]?\d{1,2}).*GET.*"

file = open("access.log", "r").read()
ips = re.findall(reg, file)
ips.sort()
answer = []
for ip in ips:
    if ip not in answer:
        if answer.__len__() > 0:
            flag = 0
            for x in range(0, 3):
                if ip.split('.')[x] != answer[answer.__len__()-1].split('.')[x]:
                    flag = 1
            if flag:
                answer.append("---")
        answer.append(ip)


for ans in answer:
    print(ans)

